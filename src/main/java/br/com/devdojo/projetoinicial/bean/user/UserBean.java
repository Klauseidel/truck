package br.com.devdojo.projetoinicial.bean.user;

import br.com.devdojo.projetoinicial.annotations.Transactional;
import br.com.devdojo.projetoinicial.bean.login.LoginBean;
import br.com.devdojo.projetoinicial.persistence.model.Driver;
import br.com.devdojo.projetoinicial.persistence.model.User;
import br.com.devdojo.projetoinicial.service.user.UserService;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
/**
 * @author william on 3/16/17.
 */
@Named
@ViewScoped
public class UserBean implements Serializable {

    private User user;
    private final LoginBean loginBean;
    private String objectId;
    private List<User> driverPendingList;
    private List<Driver> driverList;
    private String driverObjectId;

    @Inject
    public UserBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }


    public void init() {
        driverPendingList = loginBean.getUserList();
        for (int i=0; i<driverPendingList.size(); i++) {
            if (driverPendingList.get(i).getObjectId().equals(objectId)) {
                user = driverPendingList.get(i);
                break;
            }
        }
        driverList = loginBean.getDriverList();
        for (int j=0; j<driverList.size(); j++) {
            String driverKey = driverList.get(j).getName() + driverList.get(j).getPhone();
            String userKey = user.getName() + user.getPhone();
            if (driverKey.equals(userKey)) {
                driverObjectId = driverList.get(j).getObjectId();
                break;
            }
        }
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getDriverPendingList() {
        return driverPendingList;
    }

    public List<Driver> getDriverList() {
        return driverList;
    }

    public void setDriverList(List<Driver> driverList) {
        this.driverList = driverList;
    }
    public String getDriverObjectId() {
        return driverObjectId;
    }

    public void setDriverObjectId(String driverObjectId) {
        this.driverObjectId = driverObjectId;
    }

    public void setDriverPendingList(List<User> driverPendingList) {
        this.driverPendingList = driverPendingList;
    }



    @Transactional
    public String accept() throws InterruptedException, ExecutionException, TimeoutException {
   //     PerfilService.update(perfil);
        UserService.acceptDriver(objectId, driverObjectId);
        loginBean.removePendingDriver(objectId);
        return "pending?faces-redirect=true";
    }

}
