package br.com.devdojo.projetoinicial.bean.driver;

import br.com.devdojo.projetoinicial.bean.login.LoginBean;
import br.com.devdojo.projetoinicial.persistence.model.Driver;
import br.com.devdojo.projetoinicial.persistence.model.User;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author william on 3/16/17.
 */
@Named
@ViewScoped
public class DriverListBean implements Serializable {

    private List<Driver> driverList;
    private List<User> userList;
    private List<User> driverPendingList;
    private final LoginBean loginBean;
    private List<Driver> effectDriverList;

    @PostConstruct
    public void init() {
        driverPendingList = getPendingsDrivers() ;
        driverList = getDrivers();
        userList = getUsers();
    }

    public void effectiveDrivers() {
        effectDriverList = new ArrayList<>();
        for (Iterator<User> iter = userList.listIterator(); iter.hasNext(); ) {
            User u = iter.next();
            if (u.getLevel().equals("driver") && u.getDriver() != null) {
                String driverId = u.getDriver();
                driverId = driverId.split("objectId\":\"")[1];
                driverId = driverId.replace("\"}", "");
                Driver driver = new Driver();
                driver.setObjectId(driverId);
                driver.setPhone(u.getPhone());
                driver.setName(u.getName());
                driver.setEmail(u.getEmail());
                effectDriverList.add(driver);
            }
        }
    }

    @Inject
    public DriverListBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<User> getPendingsDrivers() {
        driverPendingList = loginBean.getPendingUsers();
        return driverPendingList;
    }

    public List<Driver> getDrivers() {
        driverList = loginBean.getDriverList();
        return driverList;
    }

    public List<User> getUsers() {
        userList = loginBean.getUserList();
        return userList;
    }


    public List<User> getDriverPendingList() {
        return driverPendingList;
    }

    public void setDriverPendingList(List<User> driverPendingList) {
        this.driverPendingList = driverPendingList;
    }

    public List<Driver> getDriverList() {
        return driverList;
    }

    public void setDriverList(List<Driver> driverList) {
        this.driverList = driverList;
    }

    public List<Driver> getEffectDriverList() {
        return effectDriverList;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
    public void setEffectDriverList(List<Driver> effectDriverList) {
        this.effectDriverList = effectDriverList;
    }

}
