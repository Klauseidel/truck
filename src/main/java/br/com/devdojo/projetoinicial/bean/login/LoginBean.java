package br.com.devdojo.projetoinicial.bean.login;

import br.com.devdojo.projetoinicial.persistence.model.*;
import br.com.devdojo.projetoinicial.service.customer.CustomerService;
import br.com.devdojo.projetoinicial.service.driver.DriverService;
import br.com.devdojo.projetoinicial.service.user.UserService;
import br.com.devdojo.projetoinicial.utils.FacesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author by william on 3/16/17.
 */
@Named
@SessionScoped
public class LoginBean implements Serializable {
    private boolean logged;
    private Locale locale = new Locale("pt", "BR");
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginBean.class);
    private List<User> userList;
    private List<Driver> driverList;
    private List<User> pendingUsers;
    private String username;
    private String password;

    public String logar() {
        try {
            //throws exception if not found
            //this.perfil = PerfilService.efetuarLogin(this.perfil);
            logged = true;
            loadPendingDrivers();
            return "/pages/driver/pending?faces-redirect=true";

        } catch (HttpClientErrorException e) {
            LOGGER.error("Usuário e/ou senha incorretos");
            FacesUtils.addErrorMessage("errorUserNotFound", false);
        }
        return null;
    }

    public void loadPendingDrivers() {
        pendingUsers =  new ArrayList<User>();
        userList = UserService.getAll();
        driverList = DriverService.getAll();
        for (int i = 0; i<userList.size(); i++) {
       //     if (userList.get(i).getDriver() == null) {
              if (userList.get(i).getLevel().equals("customer")) {
                  User thisUser = userList.get(i);
                    String namePhone = userList.get(i).getName() + userList.get(i).getPhone();
                    for (int j = 0; j<driverList.size(); j++) {
                        String namePhoneDriver = driverList.get(j).getName() + driverList.get(j).getPhone();
                            if (namePhone.equals(namePhoneDriver)) {
                                pendingUsers.add(thisUser);
                            }
                    }
                }
        }
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public boolean isLogged() {
        return logged;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
    public List<Driver> getDriverList() {
        return driverList;
    }

    public void setDriverList(List<Driver> driverList) {
        this.driverList = driverList;
    }

    public List<User> getPendingUsers() {
        return pendingUsers;
    }

    public void setPendingUsers(List<User> pendingUsers) {
        this.pendingUsers = pendingUsers;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void removePendingDriver(String idRemove) {

        for (Iterator<User> iter = pendingUsers.listIterator(); iter.hasNext(); ) {
            User u = iter.next();
            if (u.getObjectId().equals(idRemove)) {
                iter.remove();
            }
        }
        driverList = DriverService.getAll();
    }
}
