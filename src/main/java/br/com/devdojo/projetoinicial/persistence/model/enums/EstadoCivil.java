package br.com.devdojo.projetoinicial.persistence.model.enums;

/**
 * @author william on 3/16/17.
 */
public enum EstadoCivil {
    CASADO, DIVORCIADO,SEPARADO, SOLTEIRO, VIUVO
}
