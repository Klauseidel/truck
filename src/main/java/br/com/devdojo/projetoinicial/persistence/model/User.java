package br.com.devdojo.projetoinicial.persistence.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Esta classe representa a Cidade, contem suas caracteristicas e acoes.
 * <p>
 * Utilizar somente nas operacoes especificas.
 *
 * @author Guilherme Mendes <gmendes92@gmail.com>
 * @version 1.1.0
 * @since 1.0.0
 */
public class User extends AbstractEntity {

    private String objectId;
    private String driver;
    private String phone;
    private String name;
    private String level;
    private String email;
    private boolean emailVerify;

    public User() {
    }

    public User(int id) {
        this.setId(id);
    }

    private User(Builder builder) {
        id = builder.id;
        driver = builder.driver;
        objectId = builder.objectId;
        phone = builder.phone;
        name = builder.name;
        level = builder.level;
        email = builder.email;
        emailVerify = builder.emailVerify;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getObjectId() { return objectId; }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailVerify() {
        return emailVerify;
    }

    public void setEmailVerify(boolean emailVerify) {
        this.emailVerify = emailVerify;
    }

    public static final class Builder {

        private Integer id;
        private String driver;
        private String objectId;
        private String phone;
        private String name;
        private String email;
        private String level;
        private boolean emailVerify;
        private Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder emailVerify(boolean val) {
            emailVerify = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder level(String val) {
            level = val;
            return this;
        }
        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder driver(String val) {
            driver = val;
            return this;
        }

        public Builder objectId(String val) {
            objectId = val;
            return this;
        }

        public Builder phone(String val) {
            phone = val;
            return this;
        }


        public User build() {
            return new User(this);
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "driver='" + driver + '\'' +
                ", objectId='" + objectId + '\'' +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                ", level='" + level + '\'' +
                ", email='" + email + '\'' +
                ", emailVerify='" + emailVerify + '\'' +
                "} ";
    }
}
