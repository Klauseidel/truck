package br.com.devdojo.projetoinicial.loginfilter;

import br.com.devdojo.projetoinicial.bean.login.LoginBean;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author william on 3/16/17.
 */
public class LoginFilter implements Filter {
    @Inject
    private LoginBean loginBean;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) response;
        HttpServletRequest req = (HttpServletRequest) request;
        if (loginBean.isLogged()) {
            chain.doFilter(request, response);
        } else {
            res.sendRedirect(req.getServletContext().getContextPath() + "/login");
        }
    }

    @Override
    public void destroy() {

    }
}
