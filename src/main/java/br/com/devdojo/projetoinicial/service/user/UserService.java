package br.com.devdojo.projetoinicial.service.user;

import br.com.devdojo.projetoinicial.persistence.model.Customer;
import br.com.devdojo.projetoinicial.persistence.model.User;
import br.com.devdojo.projetoinicial.utils.ConstantsUtil;
import br.com.devdojo.projetoinicial.utils.JSONUtil;
import br.com.devdojo.projetoinicial.utils.URLUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by TruckYourWay01 on 4/16/2017.
 */
public class UserService {

    private static final String GETALL = "https://parse-server-truckyourway.herokuapp.com/1/users";
    private static final String APP_ID = "x9zL4IZEk4lhnoNmXPIFNBhhcPXBw2qZ7inQ94ny";
    private static final String REST_API_KEY = "tU8hlavk01Hf2FBD1d9EtqmUbLXEE4XxvzkrjTiV";
    private static final String MASTERKEY = "SOqNRZ3FESV69F8RLZM5NmkNj2ljGHmotRVpKB2F";


    public static List<User> getAll() {

        RestTemplate template = new RestTemplate();
        JSONObject request = new JSONObject();
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Parse-Application-Id", APP_ID);
        headers.add("X-Parse-REST-API-Key", REST_API_KEY);
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

        ResponseEntity<String> loginResponse = template.exchange(GETALL,
                HttpMethod.GET, entity, String.class);

        JSONObject object = new JSONObject(loginResponse.getBody());
        JSONArray arrayUser = object.getJSONArray("results");
        List<User> userList = new ArrayList<User>();

        for (int i = 0; i < arrayUser.length(); i++) {
            JSONObject childJSONObject = arrayUser.getJSONObject(i);
            User usr = new User();
            usr.setObjectId(childJSONObject.get("objectId").toString());
            if (childJSONObject.has("driver")) {
                usr.setDriver(childJSONObject.get("driver").toString());
        }
            if (childJSONObject.has("phone")) {
                usr.setPhone(childJSONObject.get("phone").toString());
            }
            if (childJSONObject.has("fullname")) {
                usr.setName(childJSONObject.get("fullname").toString());
            }
            if (childJSONObject.has("level")) {
                usr.setLevel(childJSONObject.get("level").toString());
            }
            if (childJSONObject.has("username")) {
                usr.setEmail(childJSONObject.get("username").toString());
            }
            if (childJSONObject.has("emailVerified")) {
                usr.setEmailVerify(childJSONObject.getBoolean("emailVerified"));
            }


            userList.add(usr);
        }

        return userList;
    }

    public static void acceptDriver(String objectId, String driverObjectId) {
        RestTemplate template = new RestTemplate();
        JSONObject request = new JSONObject();
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Parse-Application-Id", APP_ID);
        headers.add("X-Parse-REST-API-Key", REST_API_KEY);
        headers.add("Content-Type", "application/json");
        headers.add("X-Parse-Master-Key", MASTERKEY);

        JSONObject driver = new JSONObject();
        driver.put("__type", "Pointer");
        driver.put("className", "Drivers");
        driver.put("objectId", driverObjectId);

        request.put("driver", driver);
        request.put("level", "driver");
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

        ResponseEntity<String> loginResponse = template.exchange(GETALL + "/" + objectId,
                HttpMethod.PUT, entity, String.class);

    }

}
