package br.com.devdojo.projetoinicial.service.driver;

import br.com.devdojo.projetoinicial.persistence.model.Customer;
import br.com.devdojo.projetoinicial.persistence.model.Driver;
import br.com.devdojo.projetoinicial.utils.ConstantsUtil;
import br.com.devdojo.projetoinicial.utils.JSONUtil;
import br.com.devdojo.projetoinicial.utils.URLUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by TruckYourWay01 on 4/16/2017.
 */
public class DriverService {

    private static final String GETALL = "https://parse-server-truckyourway.herokuapp.com/1/classes/Drivers";
    private static final String APP_ID = "x9zL4IZEk4lhnoNmXPIFNBhhcPXBw2qZ7inQ94ny";
    private static final String REST_API_KEY = "tU8hlavk01Hf2FBD1d9EtqmUbLXEE4XxvzkrjTiV";

    public static List<Driver> getAll() {

        RestTemplate template = new RestTemplate();
        JSONObject request = new JSONObject();
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Parse-Application-Id", APP_ID);
        headers.add("X-Parse-REST-API-Key", REST_API_KEY);
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

        ResponseEntity<String> loginResponse = template.exchange(GETALL,
                HttpMethod.GET, entity, String.class);

        JSONObject object = new JSONObject(loginResponse.getBody());
        JSONArray arrayUser = object.getJSONArray("results");
        List<Driver> driverList = new ArrayList<Driver>();

        for (int i = 0; i < arrayUser.length(); i++) {
            JSONObject childJSONObject = arrayUser.getJSONObject(i);
            Driver driver = new Driver();
            driver.setObjectId(childJSONObject.get("objectId").toString());
            if (childJSONObject.has("phone")) {
                driver.setPhone(childJSONObject.get("phone").toString());
            }
            if (childJSONObject.has("name")) {
                driver.setName(childJSONObject.get("name").toString());
            }


            driverList.add(driver);
        }

        return driverList;

    }
}


//https://parse-server-truckyourway.herokuapp.com/1/classes/Drivers
