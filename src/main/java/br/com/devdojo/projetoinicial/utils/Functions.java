package br.com.devdojo.projetoinicial.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

import static br.com.devdojo.projetoinicial.utils.DateUtils.*;


/**
 * Classe de funcoes gerais
 *
 * @author Guilherme Mendes
 * @version 1.1.2
 * @since 1.0.0
 */
public class Functions {

    /**
     * Recebe uma data String no formato String yyyy-MM-dd HH:mm:ss e formata
     * para Date
     *
     * @param dateString data String no formato String yyyy-MM-dd HH:mm:ss
     * @return Um objeto Date do @param
     */
    public static Date formateDateTime(String dateString) {

        LocalDateTime parse = LocalDateTime.parse(dateString, dateTimeFormatterDBStyleWithTime);
        Date.from(parse.atZone(ZoneId.systemDefault()).toInstant());

        return Date.from(parse.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Recebe uma data String no formato String dd/mm/yyyy e formata
     * para LocalDate
     *
     * @param dateString data String no formato String dd/mm/yyyy
     * @return Um objeto Date do @param
     */
    public static LocalDate dateStringDDMMYYYToLocalDate(String dateString) {

        return LocalDate.parse(dateString, dateTimeFormatterPresentationNoTime);
    }

    /**
     * Recebe um date e retorna no formato yyyy-MM-dd HH:mm:ss
     *
     * @param date a ser formatado
     * @return Uma String contendo o Date formatado no padrao yyyy-MM-dd HH:mm:ss
     */
    public static String formateDateToString(Date date) {
        if (date == null) return null;
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).format(dateTimeFormatterDBStyleWithTime);
    }

    /**
     * Recebe um date e retorna no formato yyyy-MM-dd 23:59:59
     *
     * @param date a ser formatado
     * @return Uma String contendo o Date formatado no padrao yyyy-MM-dd 23:59:59
     */
    public static String formateDateToStringLastMinuteOfDay(Date date) {
        if (date == null) return null;
        LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).with(LocalTime.MAX);
        return localDateTime.format(dateTimeFormatterDBStyleWithTime);
    }

    /**
     * Recebe um date e retorna no formato yyyy-MM-dd 00:00:00
     *
     * @param date a ser formatado
     * @return Uma String contendo o Date formatado no padrao yyyy-MM-dd 00:00:00
     */
    public static String formateDateToStringFirstMinuteOfDay(Date date) {
        if (date == null) return null;
        LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).with(LocalTime.MIN);
        return localDateTime.format(dateTimeFormatterDBStyleWithTime);
    }

    /***
     * Recebe uma data e coloca o dia dela pro primeiro dia do mês
     * @param date Data que vai ser colocado o dia como o primeiro dia do mês
     * @return data com o dia primeiro
     */
    public static Date dateToFirstDayOfMonth(Date date) {
        LocalDateTime firstDayOfMonth = dateToLocalDateTime(date).with(TemporalAdjusters.firstDayOfMonth());
        return localDateTimeToDate(firstDayOfMonth);
    }
    /***
     * Recebe uma data e coloca o dia dela pro último dia do mês
     * @param date Data que vai ser colocado o dia como o último dia do mês
     * @return data com o último dia
     */
    public static Date dateToLastDayOfMonth(Date date) {
        LocalDateTime lastDayOfMonth = dateToLocalDateTime(date).with(TemporalAdjusters.lastDayOfMonth());
        return localDateTimeToDate(lastDayOfMonth);
    }

    /**
     * Recebe um date e retorna um LocalDate
     *
     * @param date para ser Transformado em LocalDate
     * @return LocalDate
     */
    public static LocalDate dateToLocalDate(Date date) {
        if (date == null) return null;
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalTime dateToLocalTime(Date date) {
        if (date == null) return null;
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
    }

    public static LocalDateTime dateToLocalDateTime(Date date) {
        if (date == null) return null;
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date localDateTimeToDate(LocalDateTime localDateTime) {
        if (localDateTime == null) return null;
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Recebe um LocalDate e retorna um Date
     *
     * @param localDate para ser Transformado em Date
     * @return LocalDate
     */
    public static Date localDateToDate(LocalDate localDate) {
        if (localDate == null) return null;
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /***
     * Will set the time of that date to 00:00:00
     * @param date any date
     * @return date with time 00:00:00
     */
    public static Date setTimeDateToFirstMinuteOfDay(Date date) {
        if (date == null) return null;
        LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).with(LocalTime.MIN);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /***
     * Will set the time of that date to 23:59:59
     * @param date any date
     * @return date with time 23:59:59
     */
    public static Date setTimeDateToMaxMinuteOfDay(Date date) {
        if (date == null) return null;
        LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).with(LocalTime.MAX);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }


    /**
     * Recebe um date e retorna no formato dd/MM/yyyy
     *
     * @param date a ser formatado
     * @return Uma String contendo o Date formatado no padrao dd/MM/yyyy
     */
    public static String formateDateToStringDDMMYYYY(Date date) {
        if (date == null) return null;
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).format(dateTimeFormatterPresentationNoTime);
    }

    /**
     * Recebe uma String no formato yyyy-mm-dd e retorna um Date
     *
     * @param dateYYYYMMDD String no formato yyyy-mm-dd
     * @return Um Date
     */
    public static Date formateDateStringYYYYMMDDToDate(String dateYYYYMMDD) {
        LocalDate parse = LocalDate.parse(dateYYYYMMDD, dateTimeFormatterDBStyleNoTime);
        return Date.from(parse.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Recebe uma String no formato dd/MM/yyyy e retorna um Date
     *
     * @param dateDDMMYYYY String no formato dd/MM/yyyy
     * @return Um Date
     */
    public static Date formateDateStringDDMMYYYToDate(String dateDDMMYYYY) {
        if (dateDDMMYYYY == null || dateDDMMYYYY.isEmpty()) return null;
        LocalDate parse = LocalDate.parse(dateDDMMYYYY, dateTimeFormatterPresentationNoTime);
        return Date.from(parse.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /***
     * Recebe uma data String no formato dd/MM/yyyy e retorna uma String no
     * formato yyyy-MM-dd HH:mm:ss
     *
     * @param aniversario
     *            Data String no formato dd/MM/yyyy
     * @return String no formato yyyy-MM-dd HH:mm:ss
     */
    public static String formataDataPadrao(String aniversario) {
        StringBuilder dataAniversario = new StringBuilder(19);
        if (aniversario.contains("/")) {
            String[] diaMesAnoSplit = aniversario.split("/");
            dataAniversario
                    .append(diaMesAnoSplit[2]).append("-")
                    .append(diaMesAnoSplit[1]).append("-")
                    .append(diaMesAnoSplit[0]).append(" 00:00:00");
        }
        return dataAniversario.toString().isEmpty() ? aniversario : dataAniversario.toString();
    }

    /**
     * Este metodo retorna id criado
     *
     * @param location
     * @return id
     * @since 1.1.2
     */
    public static int getLocation(String location) {
        String id = location.split("/")[3];
        id = id.split("\"]")[0];

        return Integer.parseInt(id);
    }
}