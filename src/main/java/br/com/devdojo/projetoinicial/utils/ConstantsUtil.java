package br.com.devdojo.projetoinicial.utils;

/**
 * @author william on 3/16/17.
 */
public class ConstantsUtil {
  public static final String URL_API = "http://devdojo.ddns.net:8085/api-dev";
//    public static final String URL_API = "http://localhost:8085/api-dev";
  public static final String PATTERN_YYYY_MM_DD = "yyyy-MM-dd";
  public static final String PATTERN_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
  public static final String PATTERN_DD_MM_YYYY = "dd/MM/yyyy";
  public static final String PATTERN_HH_MM = "HH:mm";
  public static final String PATTERN_MMMM_HH_MM = "MMMM HH:mm";
  public static final String PATTERN_EEEE_MMMM_HH_MM = "EEEE, dd 'de' MMMM 'as' HH:mm";

}
