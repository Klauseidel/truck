package br.com.devdojo.projetoinicial.utils;

import br.com.devdojo.projetoinicial.persistence.model.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.primefaces.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author william on 3/16/17.
 */
public class JSONUtil {
    private static final Logger logger = LoggerFactory.getLogger(JSONUtil.class);

    /***
     * Will convert the results of that url to an specific object
     * @param url API URL to make the call
     * @param clazz The class target of this JSON to be converted automatically
     * @return Will return an object T from that url automatically
     */
    public static <T> T getEntity(String url, Class<T> clazz) {
        return new RestTemplate().getForEntity(url, clazz).getBody();
    }

    /***
     * This method must be used when the json is not on the format automatically
     * and the getEntity(String url, Class<T> clazz) won be able to parse automatically
     * @param json The JSON extracted or on the format to transformed to @param clazz
     * @param clazz Type of object target of @param json
     * @return Will return an object T from that JSON
     */
    public static <T> T getEntity(String json, TypeReference<T> clazz) {
        return fromJson(json, clazz);

    }

    /***
     * Use this method when you want to send a POST request and retrieve the object
     * @param entity the object to be sent as post request
     * @param url URL POST location
     * @return the object with ID
     */
    public static <T extends AbstractEntity> T setEntity(T entity, String url) {
        ResponseEntity<T> exchange = new RestTemplate().exchange(url, HttpMethod.POST, new HttpEntity<>(entity, getHeadersJSON()), new ParameterizedTypeReference<T>() {
        });

        return exchange.getBody();
    }


    /***
     * Use this method when you want to send a POST request and retrieve the object and the return needs to be extracted
     * example {perfil:{nome:william, idade:22}}, the key is perfil in this case
     * @param key the name of the main object
     * @param url URL POST location
     * @return the object with ID
     */
    public static <T extends AbstractEntity> T setEntity(String url, String key, TypeReference<T> typeReference)  {
        ResponseEntity<String> exchange = new RestTemplate().exchange(url, HttpMethod.POST, new HttpEntity<>(getHeadersJSON()), String.class);
        return fromJson(new JSONObject(exchange.getBody()).get(key).toString(), typeReference);
    }
    /***
     * Use this method when you want to send a POST request with an entity and retrieve the object and the return needs to be extracted
     * example {perfil:{nome:william, idade:22}}, the key is perfil in this case
     * @param key the name of the main object
     * @param url URL POST location
     * @return the object with ID
     */
    public static <T extends AbstractEntity> T setEntity(T entity, String url, String key, TypeReference<T> clazz)  {
        ResponseEntity<String> exchange = new RestTemplate().exchange(url, HttpMethod.POST, new HttpEntity<>(entity,getHeadersJSON()), String.class);
        return fromJson(new JSONObject(exchange.getBody()).get(key).toString(), clazz);
    }

    /***
     * Use this method when you want to send a POST request with a List
     * @param entity the List to be sent as post request
     * @param url URL POST location
     * @return the status code of that request
     */
    public static <T extends AbstractEntity> int setEntityAndReturnStatusCode(List<T> entity, String url) {
        ResponseEntity<T> exchange = getResponseEntity(entity, url);
        return exchange.getStatusCode().value();
    }


    private static <T extends AbstractEntity> ResponseEntity<T> getResponseEntity(List<T> entity, String url) {
        return new RestTemplate().exchange(url, HttpMethod.POST, new HttpEntity<>(entity,
                getHeadersJSON()), new ParameterizedTypeReference<T>() {
        });
    }

    /***
     * Use this method when you want to send a DELETE request
     * @param entity the object to be sent as post request
     * @param url URL POST location
     * @return the status code of that request
     */
    public static <T extends AbstractEntity> int deleteEntity(T entity, String url) {
        ResponseEntity<T> exchange = new RestTemplate().exchange(url, HttpMethod.DELETE, new HttpEntity<>(entity, getHeadersJSON()), new ParameterizedTypeReference<T>() {
        });

        return exchange.getStatusCode().value();
    }
    /***
     * Use this method when you want to send a DELETE request with a List
     * @param entity the object to be sent as post request
     * @param url URL POST location
     * @return the status code of that request
     */
    public static <T extends AbstractEntity> int deleteEntity(List<T> entity, String url) {
        ResponseEntity<T> exchange = new RestTemplate().exchange(url, HttpMethod.DELETE, new HttpEntity<>(entity, getHeadersJSON()), new ParameterizedTypeReference<T>() {
        });

        return exchange.getStatusCode().value();
    }

    private static <T> T fromJson(String json, TypeReference<T> clazz) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            logger.error("Erro ao tentar realizar o parse do JSON utilizando o wrapper do jackson ", e);
            return null;
        }
    }

    public static <T> String toJson(T t) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            return mapper.writeValueAsString(t);

        } catch (Exception e) {
            logger.error("Erro ao tentar realizar a conversão de objeto para JSON utilizando o wrapper do jackson ", e);
            return null;
        }
    }

    public static String extractJSONArrayFromJSONResponse(String url, String key) {
        String entity = JSONUtil.getEntity(url, String.class);
        JSONObject objectResponse = new JSONObject(entity);
        return objectResponse.getJSONArray(key).toString();
    }

    private static HttpHeaders getHeadersJSON() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
